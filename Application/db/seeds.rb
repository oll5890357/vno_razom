User.create([
    {
      name: 'Viktor',
      email: 'Viktor@r5.ua',
      password: 11111111,
      password_confirmation: 11111111,
      role: 'admin'
    },
    {
      name: 'Alina',
      email: 'Alina@r5.ua',
      password: 11111111,
      password_confirmation: 11111111,
      role: 'student'
    },
    {
      name: 'Oleg',
      email: 'Oleg@r5.ua',
      password: 11111111,
      password_confirmation: 11111111,
      role: 'teacher'
    }
  ])
Room.create([
    {
      name: 'Math',
      owner: User.admins.sample
    },
    {
      name: 'Physics',
      owner: User.admins.sample
    },
    {
      name: 'Ukrainian',
      owner: User.admins.sample
    }
  ])
