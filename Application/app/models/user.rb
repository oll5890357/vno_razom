class User < ActiveRecord::Base
  ROLES = %w{student teacher admin}.freeze
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :rooms, dependent: :destroy
  has_many :messages, dependent: :destroy
  validates :name, presence: true
  validates :role, inclusion: { in: ROLES }

  ROLES.each do |role_name|
    define_method("#{role_name}?") do
      role_name.to_s == role.to_s
    end

    scope role_name.pluralize, -> { User.where(role: role_name) }
  end

  def in_rooms
    room_ids = $redis.get('user_rooms').presence
    room_ids = room_ids ? Marshal.load(room_ids)[self.id] : []
    Room.where(id: room_ids)
  end

  def join_room(room)
    user_rooms = $redis.get('user_rooms').presence
    user_rooms = user_rooms ? Marshal.load(user_rooms) : {}
    user_rooms[self.id] ||= []
    user_rooms[self.id].push(room.id).uniq!
    user_rooms = Marshal.dump(user_rooms)
    $redis.set("user_rooms", user_rooms)
  end

  def leave_room(room)
    user_rooms = $redis.get('user_rooms').presence
    user_rooms = user_rooms ? Marshal.load(user_rooms) : {}
    user_rooms[self.id] ||= []
    user_rooms[self.id] = user_rooms[self.id] - [room.id]
    user_rooms = Marshal.dump(user_rooms)
    $redis.set("user_rooms", user_rooms)
  end
end
