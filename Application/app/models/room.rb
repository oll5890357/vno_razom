class Room < ActiveRecord::Base
  validates :name, :owner, presence: true
  belongs_to :owner, class_name: User, foreign_key: :user_id
  has_many :messages, dependent: :destroy

  def users_online
    user_rooms = $redis.get('user_rooms').presence
    user_rooms = user_rooms ? Marshal.load(user_rooms) : {}
    user_ids   = user_rooms.select{ |k, v| v.include?(self.id) }.keys
    User.where(id: user_ids)
  end
end
