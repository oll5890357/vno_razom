class Message < ActiveRecord::Base
  belongs_to :user
  belongs_to :room
  validates :body, :user, presence: true
  default_scope -> { order('created_at ASC') }
end
