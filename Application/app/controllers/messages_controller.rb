class MessagesController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource :message
  before_action :set_message, only: [:show]

  # GET /messages
  def index
    @messages = Message.all
  end

  # GET /messages/1
  def show
  end

  # GET /messages/new
  def new
    @message = Message.new
  end

  # GET /messages/1/edit
  def edit
    @message = current_user.messages.find(params[:id])
  end

  # POST /messages
  def create
    @message = current_user.messages.new(message_params)
    @room = Room.find(params[:message][:room_id])
    if @message.save
    else
    end

    # if @message.save
    #   redirect_to @room, notice: 'Message was successfully created.'
    # else
    #   render :new
    # end
  end

  # PATCH/PUT /messages/1
  def update
    @message = current_user.messages.find(params[:id])
    if @message.update(message_params)
      redirect_to @message, notice: 'Message was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /messages/1
  def destroy
    @message = current_user.messages.find(params[:id])
    @message.destroy
    redirect_to :back, notice: 'Message was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_message
      @message = Message.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def message_params
      params.require(:message).permit(:room_id, :body)
    end
end
